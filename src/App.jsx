
import './App.scss'
import {Map} from './assets/Composant/Map/Map.jsx'

function App() {

  return (
    <>
      <div className="form">
        <div>
          <h2>Creating your itinerary</h2>
          <input type="text" id="start" placeholder="City Start" />
          <input type="text" id="end" placeholder="City end" />
          <input type="text" id="days" placeholder="Number of days" />
          <input type="text" id="steps" placeholder="Number of steps" />
          <button id="submit"><img src="./src/assets/magnifier1.svg" alt="" /></button>
        </div>
      </div>
      <div className='sidebar'>
        <h2>List of steps</h2>
        <div id="card">
          <div className='headcard'>
            <h3 className='city'>Step 1</h3>
            <p className='day'>Day 1</p>
          </div>
          <div className='bodycard'>
            <p className='activity'>Possible Activity : <br /><br />Proin fringilla massa vehicula, dictum ipsum eget, pretium lorem ..... </p>
            <button id="more">More</button>
          </div>
        </div>
        <div id="card">
          <div className='headcard'>
            <h3 className='city'>Step 1</h3>
            <p className='day'>Day 1</p>
          </div>
          <div className='bodycard'>
            <p className='activity'>Possible Activity : <br /><br />Proin fringilla massa vehicula, dictum ipsum eget, pretium lorem ..... </p>
            <button id="more">More</button>
          </div>
        </div>
        <div id="card">
          <div className='headcard'>
            <h3 className='city'>Step 1</h3>
            <p className='day'>Day 1</p>
          </div>
          <div className='bodycard'>
            <p className='activity'>Possible Activity : <br /><br />Proin fringilla massa vehicula, dictum ipsum eget, pretium lorem ..... </p>
            <button id="more">More</button>
          </div>
        </div>
        <div id="card">
          <div className='headcard'>
            <h3 className='city'>Step 1</h3>
            <p className='day'>Day 1</p>
          </div>
          <div className='bodycard'>
            <p className='activity'>Possible Activity : <br /><br />Proin fringilla massa vehicula, dictum ipsum eget, pretium lorem ..... </p>
            <button id="more">More</button>
          </div>
        </div>
        <div id="card">
          <div className='headcard'>
            <h3 className='city'>Step 1</h3>
            <p className='day'>Day 1</p>
          </div>
          <div className='bodycard'>
            <p className='activity'>Possible Activity : <br /><br />Proin fringilla massa vehicula, dictum ipsum eget, pretium lorem ..... </p>
            <button id="more">More</button>
          </div>
        </div>
      </div>
      <Map />
    </>
  )
}

export default App
